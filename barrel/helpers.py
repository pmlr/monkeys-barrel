from flask import abort

from barrel import database


def get_monkey_or_404(mnk_id: int):
    mnk = database.get_monkey(mnk_id)
    if mnk is None:
        abort(404)
    return mnk
