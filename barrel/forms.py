from flask_wtf import FlaskForm
from wtforms import validators
import wtforms


class ConfirmForm(FlaskForm):
    confirm = wtforms.BooleanField(validators=[validators.DataRequired()])


class MonkeyForm(FlaskForm):
    name = wtforms.StringField(validators=[validators.DataRequired()])

    email = wtforms.StringField(validators=[validators.DataRequired(),
                                            validators.Email()])

    age = wtforms.IntegerField(validators=[validators.DataRequired(),
                                           validators.NumberRange(min=0)])


class FilterForm(FlaskForm):
    offset = wtforms.IntegerField(validators=[validators.NumberRange(min=0)])
    sort_by = wtforms.StringField(
        default='name',
        validators=[validators.AnyOf(['name',
                                      'best_friend_name',
                                      'friends_count'])])

    sort = wtforms.StringField(default='asc',
                               validators=[validators.AnyOf(['asc', 'desc'])])
