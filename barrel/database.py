from typing import NamedTuple, Iterable
import logging

from barrel.models import Base, engine, Monkey, Session, Friendship

from sqlalchemy import func, orm
import sqlalchemy as sa


LOGGER = logging.getLogger(__name__)


def set_best_friend(monkey: Monkey, friend: Monkey):
    if not has_friendship(monkey.id, friend.id):
        message = (f'Monkey "{monkey.name}" is not friends with '
                   '"{friend.name}" to be set as best friend')
        LOGGER.warning(message)
        raise ValueError(message)

    monkey.best_friend = friend.id
    Session.object_session(monkey).commit()
    LOGGER.info(f'{monkey.name} best friend is {friend.name} now')


class NetworkMonkey(NamedTuple):
    id: int
    name: str
    has_friendship: bool


def count_monkeys() -> int:
    count = Session().query(Monkey).count()
    LOGGER.debug(f'Monkeys count is {count}')
    return count


def get_monkey_network(mnk_id: int) -> Iterable[NetworkMonkey]:
    stmts = (
        Session()
        .query(Monkey.id,
               Monkey.name,
               Friendship.rgt_monkey_id)
        .filter(Monkey.id != mnk_id)
        .outerjoin(Friendship,
                   sa.and_(Monkey.id == Friendship.lft_monkey_id,
                           mnk_id == Friendship.rgt_monkey_id))
        .order_by(Monkey.name))
    LOGGER.debug(f'Retrieving network for Monkey:{mnk_id}')
    for monkey_id, name, friendship in stmts:
        yield NetworkMonkey(id=monkey_id,
                            name=name,
                            has_friendship=friendship is not None)


def delete_monkey(mnk_id: int) -> Monkey:
    LOGGER.debug(f'Deleting Monkey:{mnk_id}')
    return (Session()
            .query(Monkey)
            .filter(Monkey.id == mnk_id)
            .delete())


def update_monkey(mnk_id: int, name: str, age: int, email: str):
    (Session()
        .query(Monkey)
        .filter(Monkey.id == mnk_id)
        .update({Monkey.name: name,
                 Monkey.age: age,
                 Monkey.email: email}))
    LOGGER.info(f'Updating Monkey:{mnk_id} to: {name}, {age}, {email}')


def get_monkey(mnk_id: int) -> Monkey:
    return Session().query(Monkey).get(mnk_id)


class MonkeyData(NamedTuple):
    monkey: Monkey
    friends_count: int
    best_friend_name: str


def get_monkeys_data(sort_by: str, sort: str, offset: int,
                     limit: int) -> Iterable[MonkeyData]:
    if sort_by not in ('name', 'best_friend_name', 'friends_count'):
        raise ValueError(f'Invalid "sort_by" value: {sort_by}')

    if sort not in ('asc', 'desc'):
        raise ValueError(f'Invalid "sort" value: {sort_by}')

    best_monkey = orm.aliased(Monkey)
    if sort_by == 'friends_count' and sort == 'asc':
        order_by = 'monkey_friends_count ASC'

    elif sort_by == 'friends_count' and sort == 'desc':
        order_by = 'monkey_friends_count DESC'

    elif sort_by == 'name' and sort == 'asc':
        order_by = Monkey.name  # type: ignore

    elif sort_by == 'name' and sort == 'desc':
        order_by = sa.desc(Monkey.name)  # type: ignore

    elif sort_by == 'best_friend_name' and sort == 'asc':
        order_by = best_monkey.name

    # Failsafe, but actually is:
    # elif sort_by == 'best_friend_name' and sort == 'desc':
    else:
        order_by = best_monkey.name.desc()

    for monkey, friends_count, best_friend_name in (
            Session()
            .query(Monkey,
                   func
                   .count(Friendship.lft_monkey_id)
                   .label('monkey_friends_count'),
                   best_monkey.name)
            .outerjoin(Friendship, Monkey.id == Friendship.lft_monkey_id)
            .outerjoin(best_monkey, Monkey.best_friend == best_monkey.id)
            .order_by(order_by)
            .group_by(Monkey, best_monkey.name)
            .limit(limit)
            .offset(offset)):
        yield MonkeyData(monkey=monkey,
                         friends_count=friends_count,
                         best_friend_name=best_friend_name or '')


def create_tables():
    Base.metadata.create_all(engine)
    LOGGER.debug('Created tables')


def drop_all_tables():
    meta = sa.MetaData()
    meta.reflect(bind=engine)
    meta.drop_all(engine)
    LOGGER.debug('Droped all tables')


def create_monkey(name: str, age: int, email: str) -> Monkey:
    session = Session()
    monkey = Monkey(name=name, age=age, email=email)
    session.add(monkey)
    session.commit()
    return monkey


def make_friendship(lft_monkey: Monkey, rgt_monkey: Monkey):
    lft_monkey, rgt_monkey = sorted([lft_monkey, rgt_monkey],
                                    key=lambda m: m.id)

    session = Session()

    if rgt_monkey not in lft_monkey.friends:  # type: ignore
        fship = Friendship(lft_monkey_id=lft_monkey.id,
                           rgt_monkey_id=rgt_monkey.id)
        session.add(fship)

    if lft_monkey not in rgt_monkey.friends:  # type: ignore
        fship = Friendship(lft_monkey_id=rgt_monkey.id,
                           rgt_monkey_id=lft_monkey.id)
        session.add(fship)

    session.commit()

    LOGGER.info(f'{lft_monkey.name} and {rgt_monkey.name} are friends now')


def destroy_friendship(lft_monkey: Monkey, rgt_monkey: Monkey):
    lft_monkey, rgt_monkey = sorted([lft_monkey, rgt_monkey],
                                    key=lambda m: m.id)

    lft_rgt_exists = has_friendship(rgt_monkey.id, lft_monkey.id)
    rgt_lft_exists = has_friendship(lft_monkey.id, rgt_monkey.id)

    if lft_rgt_exists:
        lft_monkey.friends.remove(rgt_monkey)  # type: ignore

    if rgt_lft_exists:
        rgt_monkey.friends.remove(lft_monkey)  # type: ignore

    Session.object_session(lft_monkey).commit()
    Session.object_session(rgt_monkey).commit()
    LOGGER.info(f'{lft_monkey.name} and {rgt_monkey.name} '
                'are not friends anymore')


def has_friendship(monkey_id: int, friend_id: int) -> bool:
    return Session().query(Friendship).get((monkey_id, friend_id)) is not None
