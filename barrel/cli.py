from faker import Faker
from random import randint
import click

from barrel import database


@click.command()
@click.option('--drop', is_flag=True)
def create_tables(drop):
    if drop:
        database.drop_all_tables()
        print('Tables droped!')

    database.create_tables()
    print('Tables created!')


@click.command()
@click.argument('number_of_monkeys', default=10)
def populate_tables(number_of_monkeys):
    fake = Faker()
    for i in range(number_of_monkeys):
        database.create_monkey(fake.name(), randint(18, 100), fake.email())
    print('Tables populated!')
