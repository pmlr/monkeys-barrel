from flask import Flask

from barrel import settings, cli


def make_app():
    app = Flask('monkeys')
    app.config.from_object(settings)

    from barrel.views import crud
    app.register_blueprint(crud)

    # Register command line interface
    app.cli.add_command(cli.create_tables)
    app.cli.add_command(cli.populate_tables)

    return app
