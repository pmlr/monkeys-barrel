import hashlib

from os import getenv


DATABASE_URL = getenv(
    'DATABASE_URL',
    'postgresql://postgres@localhost:5419/monkeys')

DB_ECHO = str(getenv('MONKEYS_DB_ECHO', 'false')).strip().lower() == 'true'

WTF_CSRF_ENABLED = str(getenv('WTF_CSRF_ENABLED',
                              'false')).strip().lower() == 'true'

# NOTE: Just to make sure the SECRET_KEY is something random
# (Heroku changes this URI very often)
SECRET_KEY = WTF_CSRF_SECRET_KEY = \
    hashlib.sha256(DATABASE_URL.encode()).hexdigest().upper()
