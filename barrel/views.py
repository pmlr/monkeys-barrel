from flask import (
    Blueprint, render_template, request, flash, redirect,
    url_for, abort)

from barrel.forms import MonkeyForm, ConfirmForm, FilterForm
from barrel import database, helpers


crud = Blueprint('crud', __name__, template_folder='templates')


PAGE_SIZE = 5


@crud.route('/monkey/<int:mnk_id>/friendship/<int:friend_id>/best',
            methods=['GET', 'POST'])
def set_best_friend(mnk_id: int, friend_id: int):
    if mnk_id == friend_id or not database.has_friendship(mnk_id, friend_id):
        abort(404)

    mnk = helpers.get_monkey_or_404(mnk_id)
    fmnk = helpers.get_monkey_or_404(friend_id)
    form = ConfirmForm()
    if form.validate_on_submit():
        database.set_best_friend(mnk, fmnk)

        flash(f'Monkey "{mnk.name}" and "{fmnk.name}" are best friends now.',
              'success')
        return redirect(url_for('crud.view', pk=mnk.id))

    return render_template('crud/set_best_friend.html',
                           monkey=mnk,
                           friend=fmnk,
                           form=form)


@crud.route('/monkey/<int:mnk_id>/friendship/<int:friend_id>/undo',
            methods=['GET', 'POST'])
def undo_friendship(mnk_id: int, friend_id: int):
    if mnk_id == friend_id or not database.has_friendship(mnk_id, friend_id):
        abort(404)

    mnk = helpers.get_monkey_or_404(mnk_id)
    fmnk = helpers.get_monkey_or_404(friend_id)
    form = ConfirmForm()
    if form.validate_on_submit():
        database.destroy_friendship(mnk, fmnk)
        flash(f'Monkey "{mnk.name}" are not friends "{fmnk.name}" anymore.',
              'warning')
        return redirect(url_for('crud.view', pk=mnk.id))

    return render_template('crud/undo_friendship.html',
                           monkey=mnk,
                           friend=fmnk,
                           form=form)


@crud.route('/monkey/<int:mnk_id>/friendship/<int:friend_id>/make',
            methods=['GET', 'POST'])
def make_friendship(mnk_id: int, friend_id: int):
    if mnk_id == friend_id or database.has_friendship(mnk_id, friend_id):
        abort(404)

    mnk = helpers.get_monkey_or_404(mnk_id)
    fmnk = helpers.get_monkey_or_404(friend_id)
    form = ConfirmForm()
    if form.validate_on_submit():
        database.make_friendship(mnk, fmnk)
        flash(f'Monkey "{mnk.name}" are friends "{fmnk.name}" now.', 'success')
        return redirect(url_for('crud.view', pk=mnk.id))

    return render_template('crud/make_friendship.html',
                           monkey=mnk,
                           friend=fmnk,
                           form=form)


@crud.route('/monkeys/create', methods=['GET', 'POST'])
def create():
    form = MonkeyForm(request.form)
    if form.validate_on_submit():
        mnk = database.create_monkey(form.name.data,
                                     form.age.data,
                                     form.email.data)
        flash(f'Monkey "{mnk.name}" created.', 'success')
        return redirect(url_for('crud.dashboard'))
    return render_template('crud/create.html', form=form)


@crud.route('/')
def dashboard():
    filterform = FilterForm(request.args)

    if filterform.validate():
        offset = filterform.offset.data
        sort_by = filterform.sort_by.data
        sort = filterform.sort.data
    else:
        offset = 0
        sort_by = 'name'
        sort = 'asc'

    count = database.count_monkeys()
    pages = range(0, count, PAGE_SIZE)
    monkeys = database.get_monkeys_data(sort_by, sort, offset, PAGE_SIZE)

    return render_template('crud/dashboard.html',
                           filterform=filterform,
                           monkeys=monkeys,
                           offset=offset,
                           pages=pages)


@crud.route('/monkey/<int:pk>')
def view(pk: int):
    monkey = helpers.get_monkey_or_404(pk)
    network = database.get_monkey_network(pk)
    return render_template('crud/view.html', monkey=monkey, network=network)


@crud.route('/monkey/<int:pk>/edit', methods=['GET', 'POST'])
def edit(pk: int):
    mnk = helpers.get_monkey_or_404(pk)
    form = MonkeyForm(name=mnk.name, age=mnk.age, email=mnk.email)
    if form.validate_on_submit():
        database.update_monkey(mnk.id,
                               name=form.name.data,
                               age=form.age.data,
                               email=form.email.data)
        flash(f'Monkey "{mnk.name}" updated.', 'success')
        return redirect(url_for('crud.dashboard'))

    return render_template('crud/edit.html', monkey=mnk, form=form)


@crud.route('/monkey/<int:pk>/delete', methods=['GET', 'POST'])
def delete(pk: int):
    mnk = helpers.get_monkey_or_404(pk)
    form = ConfirmForm()
    if form.validate_on_submit():
        database.delete_monkey(mnk.id)
        flash(f'Monkey "{mnk.name}" deleted.', 'warning')
        return redirect(url_for('crud.dashboard'))

    return render_template('crud/delete.html', monkey=mnk, form=form)
