import re

from sqlalchemy import orm
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.pool import NullPool
import sqlalchemy as sa

from barrel import settings


engine = sa.create_engine(settings.DATABASE_URL,
                          echo=settings.DB_ECHO,
                          poolclass=NullPool)
Session = orm.scoped_session(orm.sessionmaker(engine))
Base = declarative_base()


class Friendship(Base): # noqa
    __tablename__ = 'friendships'

    lft_monkey_id = sa.Column(
        sa.Integer,
        sa.ForeignKey('monkeys.id',
                      ondelete='CASCADE',
                      name='fk_lft_monkey_id'),
        primary_key=True)

    rgt_monkey_id = sa.Column(
        sa.Integer,
        sa.ForeignKey('monkeys.id',
                      ondelete='CASCADE',
                      name='fk_rgt_monkey_id'),
        primary_key=True)

    __table_args__ = (
        sa.CheckConstraint(lft_monkey_id != rgt_monkey_id,
                           name='ck_lft_monkey_id_rgt_monkey_id'),
        sa.UniqueConstraint(
            'lft_monkey_id',
            'rgt_monkey_id',
            name='uc_lft_monkey_id_rgt_monkey_id'),
    )


class Monkey(Base): # noqa
    __tablename__ = 'monkeys'

    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    name = sa.Column(sa.String, nullable=False)
    age = sa.Column(sa.Integer, nullable=False)
    email = sa.Column(sa.String, nullable=False)

    friends = orm.relationship(
        'Monkey',
        secondary=Friendship.__table__,
        primaryjoin=id == Friendship.__table__.c.lft_monkey_id,
        secondaryjoin=id == Friendship.__table__.c.rgt_monkey_id,
        backref='back_friends')

    best_friend = sa.Column(sa.Integer,
                            sa.ForeignKey('monkeys.id',
                                          ondelete='SET NULL',
                                          name='fk_best_friend'),
                            nullable=True)

    __table_args__ = (
        sa.CheckConstraint(best_friend != id, name='ck_monkeys_best_friend'),
    )

    def __init__(self, name: str, age: int, email: str):
        if not isinstance(name, str):
            raise TypeError('Monkey "name" should be a string')

        name = self._normalize_name(name)
        if not name:
            raise ValueError('Monkey "name" should not be empty')

        if not isinstance(age, int):
            raise TypeError('Monkey "age" should be a positive integer')

        if not age > 0:
            raise ValueError('Monkey "age" should be a positive integer')

        if not isinstance(email, str):
            raise TypeError('Monkey "email" should be a string')

        if not re.match(r'[^@]+@[^@]+\.[^@]+', email):
            raise ValueError('Monkey "email" should be a valid email')

        self.name = name
        self.age = age
        self.email = email

    @classmethod
    def _normalize_name(cls, name):
        return name.strip()

    def __repr__(self):
        return (f'<Monkey(id={self.id}, name={self.name},'
                f' age={self.age}, email={self.email})>')


orm.configure_mappers()
