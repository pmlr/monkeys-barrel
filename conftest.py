import pytest

from barrel.app import make_app
from barrel import database, models


@pytest.fixture
def db():
    database.drop_all_tables()
    database.create_tables()
    yield database
    models.Session.close_all()
    models.engine.dispose()


@pytest.fixture
def dbsession(db):
    session = models.Session()
    yield session
    session.close()


@pytest.fixture
def app():
    app = make_app()
    reqctx = app.test_request_context()
    reqctx.push()
    yield app
    reqctx.pop()
