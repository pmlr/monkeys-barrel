rundb:
	docker run -it --rm --tmpfs /var/lib/postgresql/data -p 5419:5432 -e POSTGRES_DB=monkeys postgres

test:
	pytest tests/

run:
	FLASK_APP=barrel.wsgi FLASK_DEBUG=true FLASK_ENV=development flask run

heroku-web:
	gunicorn barrel.app:make_app\(\) -b 0.0.0.0:${PORT}

heroku-release:
	FLASK_APP=barrel.wsgi FLASK_DEBUG=true flask create-tables --drop
	FLASK_APP=barrel.wsgi FLASK_DEBUG=true flask populate-tables 123
