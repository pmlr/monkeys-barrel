-r production.txt

pytest==4.3.0
pytest-cov==2.6.1
pytest-flake8==1.0.4
pytest-mypy==0.3.2
pytest-pythonpath==0.7.3
