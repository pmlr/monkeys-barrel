from barrel.forms import MonkeyForm


class TestForm:

    REQUIRED = 'This field is required.'

    NAME = 'Monkey'
    AGE = 10
    EMAIL = 'email@domain.com'

    INVALID_EMAIL = 'email_at_invalid.com'
    INVALID_EMAIL_MESSAGE = 'Invalid email address.'
    INVALID_AGE = -1
    INVALID_AGE_MESSAGE = 'Number must be at least 0.'

    def test_form_validated(self, app):
        form = MonkeyForm(name=self.NAME,
                          age=self.AGE,
                          email=self.EMAIL)

        assert form.validate()

    def test_form_required_fields(self, app):
        form = MonkeyForm()

        assert not form.validate()

        assert self.REQUIRED in form.name.errors
        assert self.REQUIRED in form.age.errors
        assert self.REQUIRED in form.email.errors

    def test_form_field_validation(self, app):
        form = MonkeyForm(name=self.NAME,
                          email=self.INVALID_EMAIL,
                          age=self.INVALID_AGE)

        assert not form.validate()

        assert not form.name.errors
        assert self.INVALID_AGE_MESSAGE in form.age.errors
        assert self.INVALID_EMAIL_MESSAGE in form.email.errors
