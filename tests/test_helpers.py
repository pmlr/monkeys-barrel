from unittest import mock

from werkzeug.exceptions import NotFound
import pytest

from barrel import helpers


def test_get_monkey_or_404():

    # Monkey does not exists on the database, abort called
    with mock.patch('barrel.database.get_monkey',
                    return_value=None):
        with pytest.raises(NotFound):
            helpers.get_monkey_or_404(1)

    # Monkey exists on the database, abort not called
    with mock.patch('barrel.database.get_monkey',
                    return_value=1):
        helpers.get_monkey_or_404(1)
