from sqlalchemy import func
import pytest

from barrel import database, models


class TestMixin:

    NAME = 'A Guy'
    AGE = 55
    EMAIL = 'aguy@domain.com'

    NAME2 = 'Joe'
    AGE2 = 22
    EMAIL2 = 'joe@domain.com'

    NAME3 = 'Doomguy'
    AGE3 = 666
    EMAIL3 = 'doom@dos.exe'

    @pytest.fixture(scope='function')
    def monkey1(self, db) -> models.Monkey:
        return database.create_monkey(self.NAME, self.AGE, self.EMAIL)

    @pytest.fixture(scope='function')
    def monkey2(self, db) -> models.Monkey:
        return database.create_monkey(self.NAME2, self.AGE2, self.EMAIL2)

    @pytest.fixture(scope='function')
    def monkey3(self, db) -> models.Monkey:
        return database.create_monkey(self.NAME3, self.AGE3, self.EMAIL3)


class TestMonkeyNetwork(TestMixin):
    def test_get_monkey_network(self,
                                monkey1: models.Monkey,
                                monkey2: models.Monkey,
                                monkey3: models.Monkey):

        database.make_friendship(monkey1, monkey2)

        assert list(database.get_monkey_network(monkey1.id)) == [
            database.NetworkMonkey(id=3,
                                   name=self.NAME3,
                                   has_friendship=False),
            database.NetworkMonkey(id=2,
                                   name=self.NAME2,
                                   has_friendship=True)]


class TestMonkeyOperations(TestMixin):

    def test_update_monkey(self, monkey1):
        database.update_monkey(monkey1.id, self.NAME2, self.AGE2, self.EMAIL2)

        mnk = database.get_monkey(monkey1.id)

        assert mnk.name == self.NAME2
        assert mnk.age == self.AGE2
        assert mnk.email == self.EMAIL2

    def test_count_monkeys(self, dbsession):
        assert database.count_monkeys() == 0

        database.create_monkey(self.NAME, self.AGE, self.EMAIL)

        assert database.count_monkeys() == 1

    def test_create_monkey(self, dbsession):
        monkey_id = database.create_monkey(self.NAME, self.AGE, self.EMAIL).id

        monkey = dbsession.query(models.Monkey).get(monkey_id)

        assert monkey.id == 1
        assert monkey.name == self.NAME
        assert monkey.age == self.AGE
        assert monkey.email == self.EMAIL

    def test_get_monkeys_data_params_sorting(self,
                                             monkey1: models.Monkey,
                                             monkey2: models.Monkey,
                                             monkey3: models.Monkey):

        database.make_friendship(monkey1, monkey2)
        database.set_best_friend(monkey1, monkey2)

        database.make_friendship(monkey2, monkey3)
        database.set_best_friend(monkey2, monkey3)

        mnkdata1 = database.MonkeyData(
            monkey=monkey1,
            friends_count=1,
            best_friend_name=self.NAME2)

        mnkdata2 = database.MonkeyData(
            monkey=monkey2,
            friends_count=2,
            best_friend_name=self.NAME3)

        mnkdata3 = database.MonkeyData(
            monkey=monkey3,
            friends_count=1,
            best_friend_name='')

        assert list(database.get_monkeys_data('name', 'asc', 0, 10)) == [
            mnkdata1,
            mnkdata3,
            mnkdata2,
        ]

        assert list(database.get_monkeys_data('name', 'desc', 0, 10)) == [
            mnkdata2,
            mnkdata3,
            mnkdata1,
        ]

        assert list(database.get_monkeys_data(
            'best_friend_name', 'asc', 0, 10)) == [
            mnkdata2,
            mnkdata1,
            mnkdata3,
        ]

        assert list(database.get_monkeys_data(
            'best_friend_name', 'desc', 0, 10)) == [
            mnkdata3,
            mnkdata1,
            mnkdata2,
        ]

        assert list(database.get_monkeys_data(
            'friends_count', 'asc', 0, 10)) == [
            mnkdata1,
            mnkdata3,
            mnkdata2,
        ]

        assert list(database.get_monkeys_data(
            'friends_count', 'desc', 0, 10)) == [
            mnkdata2,
            mnkdata1,
            mnkdata3,
        ]

    def test_get_monkeys_data(self, dbsession):
        mnk = database.create_monkey(self.NAME, self.AGE, self.EMAIL)

        monkeys = database.get_monkeys_data('name', 'asc', 0, 10)

        assert list(monkeys) == [database.MonkeyData(monkey=mnk,
                                                     friends_count=0,
                                                     best_friend_name='')]

    def test_get_monkeys_data_params_validation(self):
        with pytest.raises(ValueError) as excinfo:
            list(database.get_monkeys_data('username', 'asc', 0, 1))

        assert '"sort_by"' in str(excinfo.value)

        with pytest.raises(ValueError) as excinfo:
            list(database.get_monkeys_data('name', 'ordered', 0, 1))

        assert '"sort"' in str(excinfo.value)

    def test_delete_monkey(self, dbsession):
        monkey_id = database.create_monkey(self.NAME, self.AGE, self.EMAIL).id
        database.delete_monkey(monkey_id)

        assert not dbsession.query(models.Monkey).get(monkey_id)


class TestFriendshipOperations(TestMixin):

    def test_get_monkey_network(self, monkey1, monkey2):
        database.create_monkey(
            'Not a friend',
            3,
            'monkey3@troop.com')

        database.make_friendship(monkey1, monkey2)

        return

        network = [i for i in database.get_monkey_network(monkey1.id)]

        assert network == [
            database.NetworkMonkey(id=2,
                                   name='Joe',
                                   has_friendship=True),
            database.NetworkMonkey(id=3,
                                   name='Not a friend',
                                   has_friendship=False)]

    def test_make_friendship_called_twice(
            self,
            monkey1: models.Monkey,
            monkey2: models.Monkey):

        database.make_friendship(monkey1, monkey2)
        database.make_friendship(monkey1, monkey2)

        assert (models
                .Session()  # noqa
                .query(models.Friendship)
                .get((monkey1.id, monkey2.id)))

        assert (models
                .Session()  # noqa
                .query(models.Friendship)
                .get((monkey2.id, monkey1.id)))

    def test_make_friendship(self,
                             dbsession,
                             monkey1: models.Monkey,
                             monkey2: models.Monkey):
        database.make_friendship(monkey1, monkey2)

        assert (dbsession  # noqa
                .query(models.Friendship)
                .get((monkey1.id, monkey2.id)))

        assert (dbsession  # noqa
                .query(models.Friendship)
                .get((monkey2.id, monkey1.id)))

    def test_destroy_friendship_called_twice(
            self,
            dbsession,
            monkey1: models.Monkey,
            monkey2: models.Monkey):
        database.make_friendship(monkey1, monkey2)

        database.destroy_friendship(monkey1, monkey2)
        database.destroy_friendship(monkey1, monkey2)

        assert not (
            dbsession  # noqa
            .query(models.Friendship)
            .get((monkey1.id, monkey2.id)))

        assert not (
            dbsession  # noqa
            .query(models.Friendship)
            .get((monkey2.id, monkey1.id)))

    def test_destroy_friendship(self,
                                dbsession,
                                monkey1: models.Monkey,
                                monkey2: models.Monkey):
        database.make_friendship(monkey1, monkey2)

        database.destroy_friendship(monkey1, monkey2)

        assert not (
            dbsession  # noqa
            .query(models.Friendship)
            .get((monkey1.id, monkey2.id)))

        assert not (
            dbsession  # noqa
            .query(models.Friendship)
            .get((monkey2.id, monkey1.id)))

    def test_set_best_friend(self,
                             monkey1: models.Monkey,
                             monkey2: models.Monkey):
        database.make_friendship(monkey1, monkey2)
        database.set_best_friend(monkey1, monkey2)

        assert (models
                .Session()
                .query(models.Monkey)
                .filter(func.and_(models.Monkey.id == monkey1.id,
                                  models.Monkey.best_friend == monkey2.id)))

    def test_set_best_friend_without_friendship(self, monkey1: models.Monkey,
                                                monkey2: models.Monkey):
        with pytest.raises(ValueError):
            database.set_best_friend(monkey1, monkey2)
