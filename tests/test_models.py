import pytest
import sqlalchemy as sa

from barrel import models


class TestMixin:
    NAME = 'John Doe'
    AGE = 55
    EMAIL = 'johndoe@domain.com'

    @classmethod
    def _create_monkey(cls,
                       dbsession,
                       name: str = NAME, age: int = AGE,
                       email: str = EMAIL):
        monkey = cls._make_monkey(name=name,
                                  age=age,
                                  email=email)
        dbsession.add(monkey)
        dbsession.commit()
        return monkey

    @classmethod
    def _create_friendship(cls,
                           dbsession,
                           monkey1: models.Monkey,
                           monkey2: models.Monkey):
        fship = models.Friendship(
            lft_monkey_id=monkey1.id,
            rgt_monkey_id=monkey2.id)
        dbsession.add(fship)
        dbsession.commit()
        return fship

    @classmethod
    def _make_monkey(cls, name: str = NAME, age: int = AGE,
                     email: str = EMAIL):
        return models.Monkey(name=name, age=age, email=email)


class TestFriendship(TestMixin):
    def test_friendship_constraints_unique(self, dbsession):
        # Creates basic friendship between 2 monkeys
        mnk1 = self._create_monkey(dbsession)
        mnk2 = self._create_monkey(dbsession)

        self._create_friendship(dbsession, mnk1, mnk2)

        assert (dbsession
                .query(models.Friendship)
                .get((mnk1.id, mnk2.id)))

        # Relationship should be unique, so raises integrity error if try to
        # create another one
        with pytest.raises(sa.exc.IntegrityError):
            self._create_friendship(dbsession, mnk1, mnk2)

        dbsession.rollback()

    def test_friendship_constraints_monkey_delete(self, dbsession):
        # Creates basic friendship between 2 monkeys
        mnk1 = self._create_monkey(dbsession)
        mnk2 = self._create_monkey(dbsession)

        self._create_friendship(dbsession, mnk1, mnk2)

        assert (dbsession
                .query(models.Friendship)
                .get((mnk1.id, mnk2.id)))

        # If a Monkey is deleted from the database, the
        # friendship should be erased
        dbsession.delete(mnk1)
        dbsession.commit()

        assert not (dbsession
                    .query(models.Friendship)
                    .get((mnk1.id, mnk2.id)))

    def test_friendship_constraints_delete(self, dbsession):
        # Creates basic friendship between 2 monkeys
        mnk1 = self._create_monkey(dbsession)
        mnk2 = self._create_monkey(dbsession)

        fship = self._create_friendship(dbsession, mnk1, mnk2)

        assert (dbsession
                .query(models.Friendship)
                .get((mnk1.id, mnk2.id)))

        # If the friendship is is deleted from the database, the
        # monkeys should remain
        dbsession.delete(fship)
        dbsession.commit()

        assert (dbsession
                .query(models.Monkey)
                .get(mnk1.id))

        assert (dbsession
                .query(models.Monkey)
                .get(mnk2.id))


class TestMonkey(TestMixin):

    def test_monkey_repr(self):
        monkey = self._make_monkey()
        assert repr(monkey) == ('<Monkey(id=None, name=John Doe, age=55,'
                                ' email=johndoe@domain.com)>')

        monkey.id = 123
        assert repr(monkey) == ('<Monkey(id=123, name=John Doe, age=55,'
                                ' email=johndoe@domain.com)>')

    def test_create_monkey_validate_name_type(self, db):
        with pytest.raises(TypeError) as excinfo:
            self._make_monkey(name={'name': 'John'})

        assert 'should be a string' in str(excinfo.value)

    def test_create_monkey_validate_name_value(self, db):
        with pytest.raises(ValueError) as excinfo:
            self._make_monkey(name=' ')

        assert 'should not be empty' in str(excinfo.value)

    def test_create_monkey_validate_age_type(self, db):
        with pytest.raises(TypeError) as excinfo:
            self._make_monkey(age='10.5')

        assert 'should be a positive' in str(excinfo.value)

    def test_create_monkey_validate_age_value(self, db):
        with pytest.raises(ValueError) as excinfo:
            self._make_monkey(age=-1)

        assert 'should be a positive' in str(excinfo.value)

    def test_create_monkey_validate_email_type(self, db):
        with pytest.raises(TypeError) as excinfo:
            self._make_monkey(email={'email': 'john@doe.com'})

        assert 'should be a string' in str(excinfo.value)

    def test_create_monkey_validate_email_value(self, db):
        with pytest.raises(ValueError) as excinfo:
            self._make_monkey(email='invalid')

        assert 'should be a valid email' in str(excinfo.value)
