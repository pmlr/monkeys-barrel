from unittest import mock

from click.testing import CliRunner

from barrel import cli


@mock.patch('barrel.database.drop_all_tables')
@mock.patch('barrel.database.create_tables')
def test_create_tables_drop(create_mock, drop_mock):
    runner = CliRunner()
    result = runner.invoke(cli.create_tables, ['--drop'])

    create_mock.assert_called()
    drop_mock.assert_called()

    assert result.exit_code == 0
    assert 'droped' in result.output
    assert 'created' in result.output


@mock.patch('barrel.database.create_tables')
def test_create_tables(create_mock):
    runner = CliRunner()
    result = runner.invoke(cli.create_tables)
    create_mock.assert_called()

    assert result.exit_code == 0
    assert 'created' in result.output


@mock.patch('barrel.database.create_monkey')
def test_populate_tables(create_monkey_mock):
    count = 123

    runner = CliRunner()
    result = runner.invoke(cli.populate_tables, [f'{count}'])

    assert create_monkey_mock.call_count == count
    assert 'populated' in result.output
    assert result.exit_code == 0
